from django.shortcuts import redirect, get_object_or_404, render
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemCreate, TodoItemForm, TodoItem 

# Create your views here.
def todo_list_list (request):
    todos = TodoList.objects.all()
    context = {
        "todolist_object": todos 
    }
    return render(request, "todos/TodoList.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id =list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id) 
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "todolist_edit": todolist,
        "form": form, 
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
  todolist = TodoList.objects.get(id=id)
  if request.method == "POST":
    todolist.delete()
    return redirect("todo_list_list")
  return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemCreate(request.POST)
        if form.is_valid():
            item = form.save()            
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemCreate()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_item_update(request, id):
    edit_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edit_item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=edit_item)
    context = {
        "form": form,
        "edit_item": edit_item
        }
    return render (request, "todos/edit_item.html", context)