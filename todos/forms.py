from django.forms import ModelForm
from todos.models import TodoList, TodoItem
# from django import forms

class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]

class TodoItemCreate(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]

class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
class TodoItemEdit(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]


